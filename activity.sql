--1.
SELECT customerName 
FROM customers 
WHERE country = "Philippines";

--2.
SELECT contactLastName, contactFirstName 
FROM customers 
WHERE customerName = "La Rochelle Gifts";

--3.
SELECT productName, MSRP 
FROM products
WHERE productName = "The Titanic";

--4.
SELECT firstName, lastName 
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

--5.
SELECT customerName 
FROM customers
WHERE state IS NULL;

--6.
SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve";

--7.
SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" and creditLimit > 3000;

--8.
SELECT customerName
FROM customers
WHERE customerName NOT LIKE '%a%';

--9.
SELECT customerNumber
FROM orders
WHERE comments LIKE '%DHL%';

--10.
SELECT productLine
FROM productLines
WHERE textDescription LIKE '%state of the art%';

--11.
SELECT DISTINCT country
FROM customers;

--12.
SELECT DISTINCT status
FROM orders;

--13.
SELECT customerName, country
FROM customers
WHERE country = "USA" OR country = "France" OR country = "Canada";

--14.
SELECT employees.firstName, employees.lastName, offices.city
FROM employees JOIN offices
WHERE offices.city = "Tokyo";

--15.
SELECT customers.customerName
FROM customers JOIN employees
WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

--16.
SELECT products.productName, customers.customerName
FROM products JOIN customers
WHERE customers.customerName = "Baane Mini Imports";

--17.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices
ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

--18.
SELECT firstName, lastName
FROM employees
WHERE reportsTo = 1143;

--19.
SELECT productName, MAX(MSRP)
FROM products;

--20.
SELECT COUNT(customerName)
FROM customers
WHERE country = "UK";

--21.
SELECT productLine, COUNT(productLine) 
FROM products 
GROUP by productLine;

--22.
SELECT COUNT(customers.customerNumber), employees.employeeNumber
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber;
GROUP by employees.employeeNumber
--23.
